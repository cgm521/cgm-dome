## 一、springBoot集成jsp：
- 1、修改pom文件
```
<!--集成jsp所需jar包-->

<!--jsp页面使用jstl标签-->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
</dependency>
<!-- tomcat 的支持.-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>
<!--用于编译jsp-->
<dependency>
    <groupId>org.apache.tomcat.embed</groupId>
    <artifactId>tomcat-embed-jasper</artifactId>
    <!--<scope>provided</scope>-->
</dependency>
```

- 2、application.yml配置
```
server:
  port: 8100 
spring:
  mvc:
    view:
      prefix: /WEB-INF/jsp/   # 页面默认前缀目录
      suffix: .jsp            # 响应页面默认后缀
```
- 3、编写启动类，继承SpringBootServletInitializer类，并重写configure方法
![输入图片说明](https://gitee.com/uploads/images/2018/0506/095748_aff67e2b_1844628.png "屏幕截图.png")
这个类的作用与在web.xml中配置负责初始化Spring应用上下文的监听器作用类似，只不过在这里不需要编写额外的XML文件了。

- 4、编写Controller

![输入图片说明](https://gitee.com/uploads/images/2018/0506/103358_1741ceb2_1844628.png "屏幕截图.png")

必须是@Controller注解，如果是@RestController 则需要用new ModelAndView("first/hello");

- 5、编写jsp

![输入图片说明](https://gitee.com/uploads/images/2018/0506/103439_737c61f5_1844628.png "屏幕截图.png")

 **目录结构**

![输入图片说明](https://gitee.com/uploads/images/2018/0506/103523_c3978974_1844628.png "屏幕截图.png") 

- 6、启动项目，访问[http://localhost:8100/first](http://http://localhost:8100/first) 
![输入图片说明](https://gitee.com/uploads/images/2018/0506/103644_66669c80_1844628.png "屏幕截图.png")
可以成功访问到jsp文件。

## 注：在网上找过很多博客，都说是在main下创建一个webapp文件，设置为web，在web里创建WEB-INF/jsp,但是我这样试了很多次，一直访问不了，报错页面找不到，我这个的jsp文件是在resources目录下的META-INF/resources/WEB-INF/jsp下，否则访问不到jsp


## 二、from表单登录
- 1、pom文件与配置文件不变 代码在login文件夹下，是使用@RestController注解与ModelAndView实现访问jsp页面


## 后记
- 1、添加静态文件与css
![输入图片说明](https://gitee.com/uploads/images/2018/0506/184015_5d0c3c01_1844628.png "屏幕截图.png")
静态文件例如图片，放到resources下的static文件下，访问时以static为根目录，css可以放到jsp文件中，其他地方试了下不太行
