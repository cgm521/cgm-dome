/**
 * Choicesoft.com.cn Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.example.cgm.first;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cgm
 * @version $Id: FirstController.java, v 0.1 2018-05-05 15:37 cgm Exp $$
 */
@Controller
public class FirstController {

    @RequestMapping("/first")
    public String first(){
        return "first/hello";
    }
}
