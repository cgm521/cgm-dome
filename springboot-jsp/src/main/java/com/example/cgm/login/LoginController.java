/**
 * Choicesoft.com.cn Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package com.example.cgm.login;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author cgm
 * @version $Id: LoginController.java, v 0.1 2018-05-05 16:48 cgm Exp $$
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @GetMapping("/from")
    public ModelAndView from(){
        return new ModelAndView("login/from");
    }

    @PostMapping("/login")
    public ModelAndView login(String username, String password) {
        if (username != "" && !"".equals(password)){
            ModelAndView view = new ModelAndView("login/suss");
            view.addObject("username", username);
            view.addObject("password", password);
            return view;
        }
        return new ModelAndView("login/home");
    }
}
