package com.example.cgm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
//启动类添加Servlet支持 SpringBootServletInitializer
public class SpringbootJspApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(final SpringApplicationBuilder application) {
		return application.sources(SpringbootJspApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJspApplication.class, args);
	}
}
