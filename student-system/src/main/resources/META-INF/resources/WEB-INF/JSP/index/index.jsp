<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>index</title>
</head>

<style>
    .error {
        color: #ff0000;
        font-style: italic;
        font-weight: 900;
    }

    * {
        margin: 0;
        padding: 0;
    }

    div {
        width: 150px;
        height: 100px;
        display: table-cell;
        vertical-align: middle;
        text-align: center;
        border: 1px solid #000;
    }

    img {
        width: 1364px;
        height: 300px;
    }
</style>
<body class="label">
<div>
    <img src="index.jpg" alt="首页">
    <%--<input type="image" src="index.jpg" style="height: 500px;width: 100%"/>--%>
    <p class="error">首页</p>
</div>
</body>
</html>