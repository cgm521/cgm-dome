package com.example.cgm.dao;


import com.example.cgm.entity.User;

public interface UserDao extends Base{

    User getUserByName(String name);
}
