package com.example.cgm.dao;


import java.util.List;

public interface Base {

	<T> int save(T t);
	
	int deleteById(Long id);
	
	<T>int update(T t);
	
	<T> T selectByid(Long id);
	
	<T> List<T> selectList();
	
}
