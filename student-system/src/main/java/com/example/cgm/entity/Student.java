package com.example.cgm.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Student implements Serializable{


	private static final long serialVersionUID = 1L;
	
	private Long id;
	//姓名
	private String name;
	//学号
	private String number;
	//年龄
	private int age;
	//地址
	private String address;
	//分数
	private double grade;
	
	private Date createTime;
	
	

}
