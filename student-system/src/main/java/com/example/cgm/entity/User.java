package com.example.cgm.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	//手机号
	private String mobile;
	//邮箱
	private String mail;
	//姓名
	private String name;
	//密码
	private String password;
	
	private Date createTime;
	
	
	

}
