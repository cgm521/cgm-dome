package com.example.cgm.utils;

import java.util.Random;

public class IdUtils {

	public static Long getId() {
		Random random = new Random();
		int nextInt = random.nextInt(Integer.MAX_VALUE);
		return Long.valueOf(nextInt);
	}
	
	public static void main(String[] args) {
		System.out.println(getId());
	}
}
