package com.example.cgm.controller;

import com.example.cgm.entity.Student;
import com.example.cgm.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@PostMapping("/insert")
	public int insert(@RequestBody Student student) {
		int insert = studentService.insert(student);
		return insert;
	}
	
	@GetMapping("/get")
	public Student getById(Long id){
		return studentService.getStudebtById(id);
	}
}
