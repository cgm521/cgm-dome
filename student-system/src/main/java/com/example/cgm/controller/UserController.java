package com.example.cgm.controller;

import com.example.cgm.entity.User;
import com.example.cgm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public ModelAndView add(HttpServletRequest req) {
        String name = req.getParameter("name");
        String password = req.getParameter("password");
        User entity = new User();
        entity.setName(name);
        entity.setPassword(password);
        int insert = userService.insert(entity);

        ModelAndView view = new ModelAndView("hello");
        if (insert > 0) {
            view.addObject("insert", "登录成功");
        } else {
            view.addObject("insert", "登录失败");
        }
        return view;
    }

    @GetMapping("/get")
    public User get(Long id) {
        return userService.getUserById(id);
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

}
