package com.example.cgm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author cgm
 */
@SpringBootApplication
@MapperScan("com.example.cgm.dao")
@Controller
public class StudentSystemApplication extends SpringBootServletInitializer{
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(StudentSystemApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(StudentSystemApplication.class, args);
	}

	@GetMapping("/")
	public String index(){
		return "index/index";
	}
}
