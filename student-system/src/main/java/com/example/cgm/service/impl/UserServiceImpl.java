package com.example.cgm.service.impl;

import com.example.cgm.dao.UserDao;
import com.example.cgm.entity.User;
import com.example.cgm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao;

	@Override
	public int insert(User user) {
		User entity = userDao.getUserByName(user.getName());
		if (user.getPassword().equals(entity.getPassword())) {
			return 1;
		}

		return 0;
	}

	@Override
	public User getUserById(Long id) {
		User User = userDao.selectByid(id);
		return User;
	}

	@Override
	public int update(User User) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int delete(Long id) {
		// TODO Auto-generated method stub
		return 0;
	}

}
