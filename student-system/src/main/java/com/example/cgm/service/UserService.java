package com.example.cgm.service;


import com.example.cgm.entity.User;

public interface UserService {

	int insert(User User);
	
	User getUserById(Long id);
	
	int update(User User);
	
	int delete(Long id);
}
