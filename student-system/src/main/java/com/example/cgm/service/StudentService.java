package com.example.cgm.service;


import com.example.cgm.entity.Student;

public interface StudentService {
	
	int insert(Student student);
	
	Student getStudebtById(Long id);

}
